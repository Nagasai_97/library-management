const router = require('express').Router();

const {PrismaClient} = require('@prisma/client')

const {book} = new PrismaClient()


router.post('/',async(req,res)=>{
    const {bookname,author}= req.body;
    const createBook = await book.create({
        data:{
            bookname:bookname,
            author:author
        }
    });
    res.json(createBook)

})

router.get('/',async(req,res)=>{
    const books = await book.findMany({
        select:{
            bookname:true,
            author:true
        }
    });
    res.json(books)

})


router.put('/:id',async(req,res)=>{
    const id = parseInt(req.params.id)
    const {bookname,author} = req.body
    const updateBook = await book.update({
        where:{
            id
        },
        data:{
            bookname:bookname,
            author:author
        }
    })

    res.json(updateBook);
})

router.delete('/:id',async(req,res)=>{
    const id = parseInt(req.params.id);
    const deleteBook = await book.delete({
        where:{
            id
        }
    })
    res.json(deleteBook);



})


module.exports = router;