const router = require('express').Router();
const {PrismaClient} = require('@prisma/client')

const {book} = new PrismaClient()

router.get('/',async(req,res)=>{
    const {searchName} = req.body;
    console.log(searchName)
    const searchBooks = await book.findMany({
        where:{
            OR:[
                {bookname:{
                    startsWith: searchName,
                  }},
                {
                    author:{
                    startsWith: searchName,
                  },}
            ]
            
        }
    });
    res.json(searchBooks)

})

module.exports = router;