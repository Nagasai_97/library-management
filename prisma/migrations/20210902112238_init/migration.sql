/*
  Warnings:

  - A unique constraint covering the columns `[bookname]` on the table `Book` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[author]` on the table `Book` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `author` to the `Book` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Book` ADD COLUMN `author` VARCHAR(255) NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX `Book.bookname_unique` ON `Book`(`bookname`);

-- CreateIndex
CREATE UNIQUE INDEX `Book.author_unique` ON `Book`(`author`);
