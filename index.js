const express = require('express');
const app = express();
app.use(express.json());

app.use('/api/books',require('./routes/books'))
app.use('/api/books/:id',require('./routes/books'))
app.use('/api/search/book',require('./routes/searchBook'))


app.listen(3000,()=>{
    console.log('server started')
})